$(() => {
    kendo.ui.FilterMenu.fn.options.operators.string = {
        eq: "Equal to",
        neq: "Not equal to",
        contains: "Contains",
        doesnotcontain: "Does not Contain",
        startswith: "Starts with",
        endswith: "Ends with",
    };
    kendo.ui.FilterMenu.fn.options.operators.date = {
        eq: "Equal to",
        gt: "After",
        gte: "After or equal",
        lt: "Before",
        lte: "Before or equal",
        neq: "Not equal to",
    };
    $('#grid').kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: '/api/person/read',
                    dataType: 'json',
                    type: 'POST',
                },
                create: {
                    url: '/api/person/create',
                    dataType: 'json',
                    type: 'POST',
                },
                update: {
                    url: item => {
                        return `/api/person/update/${item.id}`
                    },
                    dataType: 'json',
                    type: 'PUT',
                },
                destroy: {
                    url: item => {
                        return `/api/person/destroy/${item.id}`
                    },
                    dataType: 'json',
                    type: 'DELETE'
                },
                parameterMap: data => {
                    return kendo.stringify(data);
                },
            },
            requestEnd: e => {
                if (e.type === 'create' || e.type === 'update') {
                    e.sender.read();
                }
            },
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: 'id',
                    fields: {
                        first_name: {
                            validation: {
                                required: true
                            }
                        },
                        last_name: {
                            validation: {
                                required: true
                            }
                        },
                        birthday: {
                            type: 'date'
                        },
                        country: {
                            defaultValue: {
                                name: ''
                            },
                        },
                        telephone: {
                            validation: {
                                required: true
                            }
                        }
                    }
                }
            },
        },
        pageable: {
            pageSize: 10,
            responsive: true,
            buttonCount: 3,
            refresh: true
        },
	mobile: true,
        editable: {
            mode: 'popup',
            create: true,
            update: true,
            destroy: true
        },
        height: 525,
        sortable: true,
        selectable: 'row',
        groupable: false,
        filterable: {
            extra: false,
        },
        toolbar: [{
            name: "create",
            text: ""
        }],
        resizable: true,
        columns: [{
                field: 'first_name',
                title: 'First Name',
		media: 'sm',
            },
            {
                field: 'last_name',
                title: 'Last Name',
            },
            {
                field: 'birthday',
                format: '{0:MM-dd-yyyy}',
		media: 'md',
                title: 'Birthday',
                format: '{0:MM-dd-yyyy}'
	    },
            {
                field: 'country',
                title: 'Country',
		media: 'md',
                editor: (container, options) => {
                    $(`<input data-text-field='name' data-value-field='id' data-bind='value:${options.field}' />`)
                        .appendTo(container)
                        .kendoComboBox({
                            index: -1,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: '/api/country/read',
                                        dataType: 'json',
                                        type: 'POST',
                                    },
                                },
                                schema: {
                                    data: 'data',
                                    total: 'total',
                                    model: {
                                        id: 'id',
                                        fields: {
                                            id: {
                                                type: 'number'
                                            },
                                            name: {
                                                type: 'string'
                                            }
                                        }
                                    }
                                }
                            }
                        });
                },
                template: x => x.country.name
            },
            {
                field: 'telephone',
                title: 'Telephone',
                editor: (container, options) => {
                    $(`<input data-text-field='name' data-value-field='id' data-bind='value:${options.field}' />`)
                        .appendTo(container)
                        .kendoMaskedTextBox({
                            mask: '0000 00 00 000'
                        });
                }
            },
        ],
        dataBound: function() {
            const grid = this;
            grid.element.off('dblclick.gridRow');
            grid.element.on('dblclick.gridRow', 'tbody tr[data-uid]', function(e) {
                grid.editRow($(e.target).closest('tr'));
            });
        }
    });
});
