<?php

namespace App;

use app\Person;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = [];
    public function persons()
    {
        return $this->hasMany(Person::class);
    }
}
