<?php

namespace App\Http\Controllers;

use App\Person;
use App\Country;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PersonController extends Controller
{

    public function index()
    {
        return Person::with(['country'])->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function read()
    {
        $person_properties = ['first_name', 'last_name', 'birthday', 'telephone'];
        $normalOperators = array(
            'eq' => '=',
            'gt' => '>',
            'gte' => '>=',
            'lt' => '<',
            'lte' => '<=',
            'neq' => '!=',
        );
        $stringOperators = array(
            'doesnotcontain' => 'NOT LIKE',
            'contains' => 'LIKE',
            'startswith' => 'LIKE',
            'endswith' => 'LIKE',
        );

        $operators = array_merge($normalOperators, $stringOperators);

        // read post parames
        $payLoad = json_decode(request()->getContent(), true);

        // set params
        $take   = $payLoad['take'] ?? null;
        $skip   = $payLoad['skip'] ?? null;
        $sort   = $payLoad['sort'] ?? null;
        $filter = $payLoad['filter'] ?? null;

        // set config
        $hasSort       = !is_null($sort) && count($sort) > 0;
        $hasPagination = !is_null($take) && !is_null($skip);
        $hasFilter     = !is_null($filter) && count($filter) > 0;

        // build the query
        $query = DB::table('people')
            ->join('countries', 'people.country_id', '=', 'countries.id');

        // handle filtering 
        if ($hasFilter) {
            $isOrLogic = $filter['logic'] == 'or';
            foreach ($filter['filters'] as $key => $value) {
                $field = in_array($value['field'], $person_properties) ? $value['field'] : Str::pluralStudly($value['field']) . '.name';
                $operand = Arr::has($person_properties, $value['operator']) ? '%' . $value['value'] . '%' : $value['value'];
                $operand = PersonController::fixOperandByOperator($value['operator'], $operand);
                $operator = $operators[$value['operator']];
                if ($key == 0 || !$isOrLogic) {
                    $query = $query->where($field, $operator, $operand);
                } else {
                    $query = $query->orWhere($field, $operator, $operand);
                }
            }
        }

        // set total count
        $count = count($query->select('people.id')->get());

        // handle sort
        if ($hasSort) {
            foreach ($sort as $value) {
                $field = in_array($value['field'], $person_properties) ? $value['field'] : $value['field'] . '_name';
                $query = $query->orderBy($field, $value['dir']);
            }
        } else {
            $query = $query->orderBy('people.updated_at', 'DESC');
        }

        // select required fields
        $query = $query->select(
            'people.*',
            'countries.id as country_id',
            'countries.name as country_name'
        );

        // handle pagination
        if ($hasPagination) {
            $query = $query->skip($skip)->take($take);
        }

        // execute query 
        $people = $query->get();

        // create the response json
        foreach ($people as &$person) {
            $person->country = [
                'id' => $person->country_id,
                'name' => $person->country_name
            ];
            unset($person->country_id, $person->country_name);
        }

        // send response
        return response()->json(['data' => $people, 'total' => $count], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payLoad = json_decode($request->getContent(), true);
        $country = is_array($payLoad['country']) ?
            Country::firstOrCreate(['name' => $payLoad['country']['name']]) : Country::firstOrCreate(['name' => $payLoad['country']]);
        $birthday = Carbon::parse($payLoad['birthday'], 'UTC');
        $person = Person::create([
            'first_name' => $payLoad['first_name'],
            'last_name' => $payLoad['last_name'],
            'telephone' => $payLoad['telephone'],
            'birthday' => $birthday,
            'country_id' => $country->id
        ]);
        return response()->json($person, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        return response()->json(Person::with(['country'])>find($person->id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Person $person)
    {
        $payLoad = json_decode($request->getContent(), true);
        $country = is_array($payLoad['country']) ?
            Country::firstOrCreate(['name' => $payLoad['country']['name']]) : Country::firstOrCreate(['name' => $payLoad['country']]);
        $birthday = Carbon::parse($payLoad['birthday'], 'UTC');
        $person->update([
            'first_name' => $payLoad['first_name'],
            'last_name' => $payLoad['last_name'],
            'telephone' => $payLoad['telephone'],
            'birthday' => $birthday,
            'country_id' => $country->id
        ]);
        return response()->json($person, 202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        $person->delete();
        return response()->json($person, 204);
    }

    private static function fixOperandByOperator(string $operator, string $operand) {
        $res = $operand;
        if ($operator == 'contains' || $operator == 'doesnotcontain') {
           $res = '%' . $operand . '%';
        } else if ($operator == 'startswith') {
           $res = $operand . '%';
        } else if ($operator == 'endswith') {
           $res = '%' . $operand;
        }
        return $res;
    }
}
