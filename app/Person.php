<?php

namespace App;

use App\Country;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $guarded = [];
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
