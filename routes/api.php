<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Person APIs
Route::post('/person/read', 'PersonController@read');
Route::post('/person/create', 'PersonController@store');
Route::put('/person/update/{person}', 'PersonController@update');
Route::delete('/person/destroy/{person}', 'PersonController@destroy');
// just for testing will be deleted next.
Route::get('person/', 'PersonController@index');

// Country APIs
Route::post('/country/read', 'CountryController@read');
Route::post('/country/create', 'CountryController@store');
Route::put('/country/update/{country}', 'CountryController@update');
Route::delete('/country/destroy/{country}', 'CountryController@destroy');
// just for testing will be deleted next.
Route::get('/country', 'CountryController@index');